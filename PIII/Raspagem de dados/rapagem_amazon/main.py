from requests_html import HTMLSession
import csv
import sqlite3
import schedule
import datetime
import time

def coleta():
    #CRIANDO O BANCO DE DADOS
    conn = sqlite3.connect('amztracker.db')
    c = conn.cursor()
    #c.execute('CREATE TABLE prices(date DATE, asin TEXT, price FLOAT, title TEXT)')

    # criando lista
    s = HTMLSession()
    asins = []

    # lendo arquivo CSV para lista
    with open('asins.csv', 'r') as f:
        csv_reader = csv.reader(f)
        for linhas in csv_reader:
            asins.append(linhas[0])

    # extraindo dados
    for asin in asins:
        r = s.get(f'https://www.amazon.com.br/dp/{asin}')
        r.html.render(sleep=1)

        try:
            preco = r.html.find('#price_inside_buybox')[0].text.replace('R$', '').replace(',', '').strip()
        except:
            preco = r.html.find('#priceblock_ourprice')[0].text.replace('R$', '').replace(',', '').strip()
        Nomeproduto = r.html.find('#productTitle')[0].text.strip()
        asin = asin
        date = datetime.datetime.today()

        c.execute('''INSERT INTO prices VALUES(?,?,?,?)''', (date, asin, preco, Nomeproduto))
        print(f'O produto {asin}, {preco} foi adiconado ao banco de dados')

    conn.commit()
    print('Fim do programa todos os produtos foram adicionado ao banco de dados')


schedule.every(5).seconds.do(coleta)

while 1:
    schedule.run_pending()
    time.sleep(1)